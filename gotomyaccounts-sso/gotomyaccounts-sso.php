<?php

/*
  Plugin Name: GoToMyAccounts Single Sign On
  Description: Provides single sign on (SSO) capabilites in a WordPress site for logged in a user to seamlessly access pages in a GoToMyAccounts customer portal.
  Version: 1.0.2
  Author: GoToMyAccounts
  Author URI: https://www.gotomyaccounts.com
  License: GPL 3.0
 */

if (!defined('ABSPATH'))
{
    exit;
}
if (!defined('GTMA_SSO_URL'))
{
    define('GTMA_SSO_URL', plugin_dir_url(__FILE__));
}

if (!defined('GTMA_SSO_PATH'))
{
    define('GTMA_SSO_PATH', plugin_dir_path(__FILE__));
}

if (!defined('GTMA_SSO_INC'))
{
    define('GTMA_SSO_INC', GTMA_SSO_PATH . "includes/");
}

function gtma_sso()
{
    if (!class_exists('GTMA_SSO'))
    {
        include(GTMA_SSO_INC . "gtma-init.php");
        new GTMA_SSO();
    }
}
add_action('plugins_loaded', 'gtma_sso');