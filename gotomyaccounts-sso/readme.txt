=== GoToMyAccounts SSO ===
Contributors: (marksauer)
Tags: QuickBooks Desktop, QuickBooks Online, GoToMyAccounts, Customer Portal, SSO 
Requires at least: 4.6
Tested up to: 5.6.2
Stable tag: 1.0.2
Requires PHP: 5.2.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The GoToMyAccounts Single Sign On plugin for WordPress allows you to have direct links into your customer portal for any logged in WordPress user.

== Description ==

The GoToMyAccounts Single Sign On plugin for WordPress allows you to have direct links into your customer portal for any logged in WordPress user. Your WordPress user's email must match the corresponding user's email address in the GoToMyAccounts web portal.

Full installation, configuration, and usage details are available in the following knowledge base article.

**[How To Setup WordPress Single Sign On (SSO)](https://help.gotomyaccounts.com/getting-started/how-to-setup-wordpress-single-sign-on-sso)**


== Installation ==
Full installation, configuration, and usage details are available in the following knowledge base article.

**[How To Setup WordPress Single Sign On (SSO)](https://help.gotomyaccounts.com/getting-started/how-to-setup-wordpress-single-sign-on-sso)**