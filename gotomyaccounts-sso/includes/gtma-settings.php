<?php
if (!defined('ABSPATH'))
{
    exit;
}
$portalURL = get_option('gtma_portal_url', '');
$sso_password = get_option('gtma_sso_password', '');
$gtma_redirects = get_option('gtma_sso_redirects', array());
$avail_pages = get_pages();
$pages[""] = 'Select Page';
foreach ($avail_pages as $page)
{
    $pages[$page->ID] = $page->post_title;
}
?>
<style>
    p.description
    {
        font-style: normal;
    }
    table.gtma_redirects td.sort::before{
        content: '\f333';
        font-family: Dashicons;
        text-align: center;
        line-height: 1;
        color: #999;
        display: block;
        width: 17px;
        float: left;
        height: 100%;
    }
    table.gtma_redirects th {
        padding: 9px 7px!important;
        vertical-align: middle;
    }
    table.gtma_redirects td.sort{
        padding: 0 7px;
        cursor: move;
        font-size: 15px;
        text-align: center;
        vertical-align: middle;
    }
    table.gtma_redirects td.name{
        font-weight: 700;
    }
    table.gtma_redirects tr:nth-child(odd) td{
        background: #f9f9f9;
    }
	.gtma-sso-settings-header-h1 {
		vertical-align:top;
	}
	
	.gtma-sso-header h1 {
	  height:90px;
	}
	
	.gtma-sso-header img {
	  float: left;
	  width: 100px;
	  height: 100px;
	  background: transparent;
	}

	.gtma-sso-header h1 {
	  position: relative;
	  top: 18px;
	  left: 10px;
	}
	
	#gtma_portal_url {
		width:400px;
	}
	
	#gtma_sso_password {
		width:500px;
	}
	
</style>

<div class="gtma-sso-header">
  <img src="https://assets.gotomyaccounts.com/images-public/logos/gtma_logo_100.png" alt="logo" />
  <h1>GoToMyAccounts Single Sign On Settings</h1>
</div>
<hr>

<div>
	<h2>
		Quick-Start Instructions
	</h2>
	<strong>Note: In order for SSO to work, your logged in WordPress user must have a corresponding web portal account with the exact same email address.</strong>
	<ol>
		<li>Set your web portal address and SSO token</li>
		<li>
			Create empty pages for the portal redirects you desire. (Ex: Billing, Make a payment, Manage Users, etc)<br>
			(These represent a single "deep link" into your web portal)
		</li>
		<li>Add your pages (links) to one or menus.</li>
		<li>Click a menu item to test.</li>
	</ol>
	<h3>
		<a target="_blank" href="https://help.gotomyaccounts.com/getting-started/how-to-setup-wordpress-single-sign-on-sso">Full Documentation</a>
	</h3>
</div>
<hr>

<div class="wrap" style="padding: 0px 10px 0px 10px;">
    <form method="POST" action="<?php echo admin_url("options-general.php?page=gtma_sso"); ?>">
        <table class="form-table">
            <tr>
                <th scope="row">
                    <label for="gtma_portal_url">
                        GoToMyAccounts Portal URL
                    </label>
                </th>
                <td>
                    <input name="gtma_portal_url" type="text" id="gtma_portal_url" placeholder="Portal URl" value="<?php echo $portalURL; ?>">
                    <p class="description">
						Set your GoToMyAccounts portal URL (no http/https or trailing "/").<br>
						<strong>Example</strong>: myportal.gotomyaccounts.com
					</p>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="gtma_sso_password">
                        SSO Token
                    </label>
                </th>
                <td>
                    <input name="gtma_sso_password" type="password" id="gtma_sso_password" placeholder="SSO Password" value="<?php echo $sso_password; ?>">
                    <p class="description">
						<strong>Where do I find this?</strong> You can manage your SSO tokens from within your portal (when logged in with admin permissions). On the left 
						menu, click Integrations, then click "SSO Tokens". Copy a token from that page and paste it into this field.
					</p>
                </td>
            </tr>
			
            <tr valign="top">
                <th scope="row">
                    Add Redirect
                </th>
                <td colspan="2">
                    <select name="redirect[page]" style="width: 35%;">
                        <?php
                        foreach ($pages as $id => $title)
                        {
                            echo '<option value="' . $id . '">' . $title . '</option>';
                        }
                        ?>
                    </select>
                    <input type="text" name="redirect[address]" size="43" placeholder="Add Web Portal Redirect Page">
					<br />
					Actual Wordpress pages should not contain any content. The plugin will cause these pages to redirect instead to your Web Portal Redirect Page.<br>
					If the redirect fails, the page will instead display an error message to the user.<br>
					Example Web Portal Redirect Pages: "customer_dashboard.html", "payment.html", "customer_manage_users.html"
                </td>
            </tr>
			
        </table>
        <input type="submit" class="button button-primary" name="gmta_sso_submit" id="gmta_sso_submit" value="Save changes">
        <hr style="margin-top: 1.5em;" >
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row" class="titledesc">Redirected Page</th>
                    <td class="forminp">
                        <table class="gtma_redirects widefat" cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="page">WordPress Page (should be empty)</th>
                                    <th class="redirect">Web Portal Redirect Page</th>
                                    <th class="actions">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="ui-sortable">
                                <?php
                                if (!empty($gtma_redirects))
                                {
                                    foreach ($gtma_redirects as $id => $redirect)
									{
										if ($id) 
										{
											?>
												<tr>
													<td class="page">
														<?php echo $pages[$id]; ?>
													</td>
													<td class="redirect"><?php echo $redirect ?></td>
													<td class="actions"><a href="<?php echo admin_url("options-general.php?page=gtma_sso&action=delete&id=" . $id); ?>"><span class="dashicons dashicons-trash"></span></a></td>
												</tr>
                                        	<?php	
										}
                                    }
                                } else
                                {
                                    ?>
                                    <tr>
                                        <td colspan="4">No Redirects Configured</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>